package com.openclassrooms.tourguide.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.stereotype.Service;

import com.openclassrooms.tourguide.user.User;
import com.openclassrooms.tourguide.user.UserReward;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;

@Service
public class RewardsService {
	private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;

	// proximity in miles
	private int defaultProximityBuffer = 10;
	private int proximityBuffer = defaultProximityBuffer;
	private int attractionProximityRange = 200;
	private final GpsUtil gpsUtil;
	private final RewardCentral rewardsCentral;

	// private int iUser = 1;
	// private ServiceJ serviceJ = new ServiceJ();

	private CompletableFuture<Void> myFuture = new CompletableFuture<>();
	private CompletableFuture<Void> myFutureMF = new CompletableFuture<>();

	private int threads = Runtime.getRuntime().availableProcessors();

	public RewardsService(
			GpsUtil gpsUtil, RewardCentral rewardCentral) {
		this.gpsUtil = gpsUtil;
		this.rewardsCentral = rewardCentral;
	}

	public void setProximityBuffer(int proximityBuffer) {
		this.proximityBuffer = proximityBuffer;
	}

	public void setDefaultProximityBuffer() {
		proximityBuffer = defaultProximityBuffer;
	}

	public void calculateRewards(User user) {

		CompletableFuture<Void> myFuture2 = new CompletableFuture<>();
		CompletableFuture<Void> myFuture3 = new CompletableFuture<>();

		ArrayList<CompletableFuture> futures = new ArrayList<>();

		ExecutorService executorService = Executors.newFixedThreadPool(threads);
		// List<CompletableFuture<Void>> futures = new ArrayList<>();

		// LocalTime localTime = LocalTime.now();
		// System.out.println("Je passe"
		// + " user ID : " + iUser);
		// iUser = iUser + 1;

		// Executors.newCachedThreadPool().submit(() -> {

		futures.add(
				CompletableFuture.runAsync(() -> {

					// List<VisitedLocation> userLocations = user.getVisitedLocations();
					CopyOnWriteArrayList<VisitedLocation> userLocations = new CopyOnWriteArrayList<>();
					userLocations.addAll(user.getVisitedLocations());

					// List<Attraction> attractions = gpsUtil.getAttractions();
					CopyOnWriteArrayList<Attraction> attractions = new CopyOnWriteArrayList<>();
					attractions.addAll(gpsUtil.getAttractions());

					for (VisitedLocation visitedLocation : userLocations) {
						for (Attraction attraction : attractions) {
							if (user.getUserRewards().stream()
									.filter(r -> r.attraction.attractionName.equals(attraction.attractionName))
									.count() == 0
									&& (nearAttraction(visitedLocation, attraction))) {
								user.addUserReward(
										new UserReward(visitedLocation, attraction,
												getRewardPoints(attraction, user)));
								// serviceJ.setServiceJ(serviceJ.getServiceJ() + 1);
								// System.out.println(
								// "new user rewars added :" + serviceJ.getServiceJ());
								myFuture.complete(null);
							}

						}
					}
					// myFuture3.complete(null);
				}, executorService));

		futures.forEach((n) -> {
			try {
				n.get();
				// myFuture.get();
			} catch (InterruptedException e) {
				// logger.error("Calculate Rewards InterruptedException: " + e);
			} catch (ExecutionException e) {
				// logger.error("Calculate Rewards ExecutionException: " + e);
			}
		});

		executorService.shutdown();

		// myFuture2.complete(null);

		// executorService.shutdown();

		/* */
		// List<VisitedLocation> userLocations = user.getVisitedLocations();
		// List<Attraction> attractions = gpsService.getAttractions();

		// ArrayList<CompletableFuture> futures = new ArrayList<>();

		// for (VisitedLocation visitedLocation : userLocations) {
		// for (Attraction attr : attractions) {
		// futures.add(
		// CompletableFuture.runAsync(() -> {
		// if (user.getUserRewards().stream()
		// .filter(r -> r.attraction.attractionName.equals(attr.attractionName))
		// .count() == 0) {
		//
		// if (nearAttraction(visitedLocation, attr)) {
		// userService.addUserReward(user.getUserName(), visitedLocation, attr,
		// getRewardPoints(attr, user.getUserId()));
		// }
		// }
		// }, executorService));
		// }
		// }

		// futures.forEach((n) -> {
		// try {
		// n.get();
		// } catch (InterruptedException e) {
		// logger.error("Calculate Rewards InterruptedException: " + e);
		// } catch (ExecutionException e) {
		// logger.error("Calculate Rewards ExecutionException: " + e);
		// }
		// });

		// executorService.shutdown();

		// CompletableFuture.allOf(futures.toArray(new CompletableFuture[0])).join();
		// executorService.shutdown();

		/*
		 * 
		 * 
		 * Executors.newCachedThreadPool().submit(() -> {
		 * CompletableFuture.supplyAsync(() -> {
		 * 
		 * return null;
		 * });
		 * });
		 * 
		 * for (VisitedLocation visitedLocation : userLocations) {
		 * for (Attraction attraction : attractions) {
		 * if (user.getUserRewards()
		 * .stream()
		 * .filter(r -> r.attraction.attractionName.equals(attraction.attractionName))
		 * .count() == 0
		 * && (nearAttraction(visitedLocation, attraction))) {
		 * user.addUserReward(
		 * new UserReward(visitedLocation, attraction,
		 * getRewardPoints(attraction, user)));
		 * }
		 * }
		 * 
		 * }
		 */
	}

	public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
		return getDistance(attraction, location) > attractionProximityRange ? false : true;

	}

	private boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
		return getDistance(attraction, visitedLocation.location) > proximityBuffer ? false : true;
	}

	private int getRewardPoints(Attraction attraction, User user) {
		return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
	}

	public double getDistance(Location loc1, Location loc2) {
		double lat1 = Math.toRadians(loc1.latitude);
		double lon1 = Math.toRadians(loc1.longitude);
		double lat2 = Math.toRadians(loc2.latitude);
		double lon2 = Math.toRadians(loc2.longitude);

		double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
				+ Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

		double nauticalMiles = 60 * Math.toDegrees(angle);
		double statuteMiles = STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
		return statuteMiles;
	}

}
