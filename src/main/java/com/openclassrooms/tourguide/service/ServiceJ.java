package com.openclassrooms.tourguide.service;

public class ServiceJ {

    private int serviceJ;

    public ServiceJ() {
    }

    public ServiceJ(int serviceJ) {
        this.serviceJ = serviceJ;
    }

    public int getServiceJ() {
        return serviceJ;
    }

    public void setServiceJ(int serviceJ) {
        this.serviceJ = serviceJ;
    }

}
