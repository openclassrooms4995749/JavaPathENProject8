package com.openclassrooms.tourguide.service;

public class ServiceI {

    private int serviceI;

  public ServiceI() {
    }

    public ServiceI(int serviceI) {
        this.serviceI = serviceI;
    }

    

    public int getServiceI() {
        return serviceI;
    }

    public void setServiceI(int serviceI) {
        this.serviceI = serviceI;
    }

}
